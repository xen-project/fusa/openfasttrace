#!/bin/bash -e

BRANCH_NAME="$1"


git clone --depth 1 https://gitlab.com/xen-project/xen.git -b "$BRANCH_NAME"

set +e

java -jar ./binaries/oft.jar trace ./xen

java -jar ./binaries/oft.jar trace ./xen -o html -f report.html -a XenMkt,XenProd,XenSwdgn

set -e

exit 0
