ARG base_image=ubuntu:22.04
FROM $base_image

LABEL maintainer.name="The Xen Project" \
      maintainer.email="xen-devel@lists.xenproject.org"

ENV DEBIAN_FRONTEND=noninteractive
ENV USER root

RUN mkdir /build
WORKDIR /build

RUN <<EOF
#!/bin/bash

    set -e

    apt-get update

    apt-get --quiet --yes install

    DEPS=(
        openjdk-17-jdk
        maven
        git
    )

    apt-get autoremove -y
    apt-get clean
    rm -rf /var/lib/apt/lists* /tmp/* /var/tmp/*
EOF
