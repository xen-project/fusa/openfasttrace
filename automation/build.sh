#!/bin/bash -e

mkdir -p binaries
mkdir -p ~/.m2

# Configure Maven Toolchain
echo '
<?xml version="1.0" encoding="UTF8"?>
<toolchains>
    <toolchain>
        <type>jdk</type>
        <provides>
            <version>17</version>
        </provides>
        <configuration>
            <jdkHome>/usr/</jdkHome>
        </configuration>
    </toolchain>
</toolchains>
' > ~/.m2/toolchains.xml

# Build
mvn clean package -T 1C -DskipTests

version=$(grep -zo '<revision>.*</revision>' ./parent/pom.xml | sed -e's/[^0-9.]//g')

# Copy JAR
cp product/target/openfasttrace-${version}.jar binaries/oft.jar
